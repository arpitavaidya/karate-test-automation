# Karate Test Automation Project

This project demonstrates test automation using Karate for testing Rijksmuseum APIs.


## Automated APIs

This project automates testing for the following APIs provided by the Rijksmuseum:

### 1. Collection API

- **Endpoint:** `/api/[culture]/collection`
- **Description:** Gives access to the collection with brief information about each object. The results are split up into result pages. By using the `p` and `ps` parameters, you can fetch more results, up to a total of 10,000. All of the other parameters are identical to the advanced search page on the Rijksmuseum website. You can use that page to find out what’s the best query to use.

#### Scenarios Covered:

- **Invalid Authentication:** Tests the API response when invalid authentication credentials are provided.
- **Param Validation:** Validates the behavior of the API when correct parameters are provided.
- **Page Param Validation:** Validates the pagination feature, including assertions on the page object.
- **Schema Validation:** Ensures that the API response adheres to the expected schema.
- **Success Validation:** Validates the success response, including assertions on the involved maker and the count of objects.

### 2. Collection Details API

- **Endpoint:** `/api/[culture]/collection/[object-number]`
- **Description:** Gives more details of an object. Object numbers can be obtained from the results given in the Collection API.

#### Scenarios Covered:

- **Invalid Authentication:** Tests the API response when invalid authentication credentials are provided.
- **Schema Validation:** Ensures that the API response adheres to the expected schema.
- **Success Response Validation:** Validates the success response, including assertions on data values like the name of the object and title.

### 3. Collection Image API

- **Endpoint:** `/api/[culture]/collection/[object-number]/image`
- **Description:** Provides access to the image of a specific object.

#### Scenarios Covered:

- **Invalid Authentication:** Tests the API response when invalid authentication credentials are provided.
- **Schema Validation:** Ensures that the API response adheres to the expected schema.
- **Success Response Validation:** Validates the success response, including assertions on the image data.


## Prerequisites

Make sure you have the following installed on your machine:

- Java
- Maven

## Getting Started

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/arpitavaidya/karate-test-automation.git


2. Navigate to the project directory:

   ```bash
   cd karate-test-automation

3. Build the project using Maven:

   ```bash
   mvn clean install
   
4. Run the tests using the following Maven command:
   ```bash
   mvn test -Dkarate.env=TEST -Dtest=KarateRunner "-Dkarate.options=--tags @regression"
This command will execute all the Karate tests in the project.


## Project Structure
The project structure follows a standard Maven directory layout:

      |-- src
      |   |-- test
      |       |-- java
      |           |-- features        # Karate feature files
      |           |-- examples        # Sample data and utility functions
      |           |-- testrunner      # Karate test runner files
      |-- pom.xml                     # Maven configuration file
      |-- README.md                   # Project documentation
      
## Karate Features
Karate feature files are located in the src/test/java/features directory. They are written in Gherkin syntax and provide readable test scenarios.

## Configuration
Karate test runner files, located in the src/test/java/testrunner directory, define the test execution configurations and hooks.

## Reporting
Karate generates detailed HTML reports that can be found in the target/surefire-reports directory after running the tests.

![img.png](img.png)

## References
   - Karate Documentation
   - Karate GitHub Repository

## Observations
   - In case of invalid object number which does not exist is responding in 500 which is not expected also happening for image api

## Improvements
   - Implementation of data files as per the env
   - Implementation of performance test using Gatling
