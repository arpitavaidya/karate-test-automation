@collection-api-ft @regression
Feature: Validate the working of RijksMuseum data collection api

  Background:
    * def invalidCulture = 'BE'
    * def collectionSchema = read('../__files/collection-api-get-response.json')
    * url baseUrl
    * path culture+'/collection'

  @tc_01 @collection-invalid-key
  Scenario: Validate api must respond with 401 in case of invalid apiKey

    Given param key = 'invalid-key'
    When method get
    Then status 401

  @tc_02 @collection-missing-key
  Scenario: Validate api must respond with 401 in case of apiKey is missing

    When method get
    Then status 401

  @tc_03 @collection-invalid-culture
  Scenario: Validate api must respond with 404 in case of invalid culture

    Given path invalidCulture+'/collection'
    Given param key = apiKey
    When method get
    Then status 404

  @tc_04 @collection-success @sanity
  Scenario Outline: validate the following working of the collection api
                      - response must have status 200
                      - response must adhere to the expected schema
                      - response must have the expected count
                      - response must contain artobjects
                      - the object principalOrFirstMaker must be same as  <involvedMaker>

    Given param key = apiKey
    And param involvedMaker = '<involvedMaker>'
    When method get
    Then status 200
    And match response == collectionSchema
    And match each response.artObjects[*].principalOrFirstMaker == '<involvedMaker>'
    And match response.count == <totalCount>

    Examples:
      | involvedMaker      | totalCount |
      | Rembrandt van Rijn | 3672       |


  @tc_05 @collection-param @success
  Scenario Outline: Validates the behavior of the API when correct query parameters are provided.

    Given param key = apiKey
    And param <param_name_1> = '<param_value>'
    When method get
    Then status 200
    And match response == '#present'
    And assert response.count >= 0

    Examples:
      | param_name_1            | param_value |
      | format                   | json        |
      | q                        | painting    |
      | type                     | painting    |
      | material                 | canvas      |
      | technique                | painting    |
      | f.dating.period          | 20          |
      | f.normalized32Colors.hex | %23FF0000   |
      | imgonly                  | true        |
      | toppieces                | true        |
      | s                        | relevance   |


  @tc_06   @collection-page
  Scenario Outline: Validates the pagination feature, including assertions on the page object

    Given param key = apiKey
    And param involvedMaker = 'Rembrandt van Rijn'
    And param p = <pageNum>
    And param ps = <noOfObj>
    When method get
    Then status 200
    And match response.count == <totalCount>
    And match response.artObjects == '#[<numberOfObjectInPage>]'

    Examples:
      | pageNum | noOfObj | totalCount | numberOfObjectInPage |
      | 1       | 10      | 3672       | 10                   |
      | 368     | 10      | 3672       | 2                   |