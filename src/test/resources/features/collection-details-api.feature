@collection-details-api-ft @regression
Feature: Validate the working of RijksMuseum data collection details api

  Background:
    * def objectNumber = 'SK-C-5'
    * def collectionDetailsSchema = read('../__files/collection-details-api-response.json')
    * url baseUrl
    * path culture+'/collection/'+objectNumber

  @tc_07 @collection-details-invalid-key
  Scenario: Validate api must respond with 401 in case of invalid apiKey

    Given param key = 'invalid-key'
    When method get
    Then status 401

  @tc_08 @collection-details-missing-key
  Scenario: Validate api must respond with 401 in case of apiKey is missing in the request

    When method get
    Then status 401

  @tc_09 @collection-details-schema-validation-ft
  Scenario: Validate api response must adheres to the expected schema

    Given param key = apiKey
    When method get
    Then status 200
    And match response == collectionDetailsSchema


  @tc_10 @collection-details-success
  Scenario: validate the following working of the collection api
            - response must have status 200
            - response must contains objectNumber and have expected value
            - response must contains title and have expected value

    Given param key = apiKey
    When method get
    Then status 200
    And assert response.artObject.objectNumber === objectNumber
    And assert response.artObject.title == 'The Night Watch'









