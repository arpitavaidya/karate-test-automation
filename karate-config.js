function fn() {
    var env = karate.env; // get java system property 'karate.env'
    karate.log('karate.env system property was:', env);
    if (!env) {
        env = 'DEV'; // a custom 'intelligent' default
    }
    var config = { // base config JSON
        apiKey: '0fiuZFh4',
        baseUrl: 'https://www.rijksmuseum.nl/api/',
        culture: 'en'
    };
    if (env == 'TEST') {
        // over-ride only those that need to be
        config.apiKey = '0fiuZFh4';
        config.baseUrl = 'https://www.rijksmuseum.nl/api/';
        config.culture = 'en';
    } else if (env == 'ACC') {
        config.apiKey = '0fiuZFh4';
        config.baseUrl = 'https://www.rijksmuseum.nl/api/';
        config.culture = 'en';
    }
    // don't waste time waiting for a connection or if servers don't respond within 5 seconds
    karate.configure('connectTimeout', 5000);
    karate.configure('readTimeout', 5000);
    return config;
}