@collection-image-api-ft @regression
Feature: Validate the working of RijksMuseum data collection image api

  Background:
    * def objectNumber = 'SK-C-5'
    * def expectedLevelNames = ['z0','z1','z2','z3','z4','z5','z6'];
    * url baseUrl
    * path culture+'/collection/'+objectNumber+'/tiles'

  @tc_11 @collection-image-invalid-key
  Scenario: Validate api must respond with 401 in case of invalid apiKey

    Given param key = 'invalid-key'
    When method get
    Then status 401

  @tc_12 @collection-image-missing-key
  Scenario: Validate api must respond with 401 in case of apiKey is missing

    When method get
    Then status 401

  @tc_13 @collection-image-schema-validation
  Scenario: Validate api response must adheres to the expected schema

    Given param key = apiKey
    When method get
    Then status 200
    And match response == { levels: '#array'}
    And match each response.levels[*] == {name: '#string', width: '#number', height: '#number', tiles: '#array'}


  @tc_14 @@collection-image-artObject
  Scenario: Validate api must respond with 200 and the levels name must be between Z0 to Z6

    Given param key = apiKey
    When method get
    Then status 200
    And match karate.get('$response.levels[*].name') contains only expectedLevelNames











