package com.rijksmuseum.karate;
import com.intuit.karate.Results;
import com.intuit.karate.Runner;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
public class KarateRunner {
    @Test
    void testParallel() {
        String karateOutputPath = "target/surefire-reports";
        Results results = Runner.path("src/test/resources/features").tags("~@skipme").reportDir(karateOutputPath).outputCucumberJson(true).parallel(5);
        assertEquals(0, results.getFailCount(), results.getErrorMessages());
    }
}
